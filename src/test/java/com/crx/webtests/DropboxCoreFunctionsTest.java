package com.crx.webtests;

import java.io.File;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.crx.webtests.core.DropboxUserSteps;
import com.crx.webtests.requirements.Application;

@RunWith(ThucydidesRunner.class)
@Story(Application.DropBox.CoreFunctions.class)
public class DropboxCoreFunctionsTest {

	@Managed(driver = "chrome")
	public WebDriver webdriver;

	@Steps
	public DropboxUserSteps dropboxUser;

	@ManagedPages
	public Pages pages;

	@Before
	public void setUp() throws Exception {
		webdriver.manage().window().maximize();
	}

	@Test
	public void e2eTest() {

		String login = System.getProperty("dropbox.login");
		String password = System.getProperty("dropbox.password");

		String folderName = "test_folder";
		String fileName = "test_file.txt";
		String filePath = new File("src/test/resources/" + fileName)
				.getAbsolutePath();
		String newFileName = "new_test_file";

		dropboxUser.openStartPage();
		dropboxUser.signIn(login, password);

		dropboxUser.createFolder(folderName);
		dropboxUser.assertFileInList(folderName);

		dropboxUser.uploadFile(filePath);
		dropboxUser.assertFileInList(fileName);

		dropboxUser.navigateToEvents();
		dropboxUser.checkEvent(folderName, "folder", 1);
		dropboxUser.checkEvent(fileName, "file", 0);

		dropboxUser.navigateToFiles();
		dropboxUser.renameFile(fileName, newFileName);
		dropboxUser.assertFileInList(newFileName + ".txt");
		dropboxUser.assertFileNotInList(fileName);

		dropboxUser.deleteFile(newFileName + ".txt");
		dropboxUser.assertFileNotInList(newFileName + ".txt");

		dropboxUser.deleteFile(folderName);
		dropboxUser.assertFileNotInList(folderName);

	}

}
