package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Upload' popup window.
 */
public class UploadPage extends PageObject {

	@FindBy(id = "choose-button")
	private WebElement chooseBtn;

	private String fileInputXpath = "//input[@type='file']";

	private String doneBtnXpath = "//input[@value='Done']";

	private WebDriverWait wait = new WebDriverWait(super.getDriver(), 30);

	/**
	 * Constructor. Used by Thucydides.
	 */
	public UploadPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(chooseBtn).waitUntilVisible();
	}

	/**
	 * Clicks 'Choose Files' button
	 */
	public void clickChooseBtn() {
		chooseBtn.click();
	}

	/**
	 * Uploads file and cliks 'Done' button to close popus window.
	 * 
	 * @param filePath
	 *            - absolute path to file in the system.
	 */
	public void uploadFileAndClickDone(String filePath) {
		super.getDriver().findElements(By.xpath(fileInputXpath)).get(0)
				.sendKeys(filePath);
		WebElement doneBtn = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(doneBtnXpath)));
		doneBtn.click();
	}
}
