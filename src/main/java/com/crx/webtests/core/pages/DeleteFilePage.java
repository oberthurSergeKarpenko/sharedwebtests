package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Delete File' popup window.
 */
public class DeleteFilePage extends PageObject {

	@FindBy(xpath = "//div[@class='db-modal-buttons']/button[text()='Delete']")
	private WebElement deleteBtn;

	/**
	 * Constructor. Used by Thucydides.
	 */
	public DeleteFilePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(deleteBtn).waitUntilVisible();
	}

	/**
	 * Clicks 'Delete' button
	 */
	public void clickDeleteBtn() {
		deleteBtn.click();
	}

}
