package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author serge
 * 
 *         Page Object for Dropbox start page.
 */
@DefaultUrl("https://www.dropbox.com/")
public class StartPage extends PageObject {

	@FindBy(id = "sign-in")
	private WebElement signInLink;

	/**
	 * Constructor. Used by Thucydides.
	 */
	public StartPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(signInLink).waitUntilVisible();
	}

	/**
	 * Clicks 'Sign In' link.
	 */
	public void clickSignInLink() {
		signInLink.click();
	}

}
