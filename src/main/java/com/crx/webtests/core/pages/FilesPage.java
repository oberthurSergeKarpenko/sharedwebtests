package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Files' page.
 */
public class FilesPage extends PageWithSideBar {

	@FindBy(id = "new_folder_button")
	private WebElement newFolderBtn;

	@FindBy(id = "upload_button")
	private WebElement uploadBtn;

	private WebDriverWait wait = new WebDriverWait(super.getDriver(), 30);

	/**
	 * Constructor. Used by Thucydides.
	 */
	public FilesPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(uploadBtn).waitUntilVisible();
	}

	/**
	 * Clicks 'New Folder' button.
	 */
	public void clickNewFolderBtn() {
		newFolderBtn.click();
	}

	/**
	 * Clicks 'Upload' button.
	 */
	public void clickUploadBtn() {
		uploadBtn.click();
	}

	/**
	 * Checks if file is in the file list.
	 * 
	 * @param fileName
	 *            - name of the file
	 * @return - true if file was found in the list, else false
	 */
	public boolean isFileInList(String fileName) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.linkText(fileName)));
		return super.getDriver().findElements(By.linkText(fileName)).size() == 1;
	}

	/**
	 * Checks if file is not in the file list.
	 * 
	 * @param fileName
	 *            - name of the file
	 * @return - true if file not present in the list, else false
	 */
	public boolean isFileNotInList(String fileName) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By
				.linkText(fileName)));
		return super.getDriver().findElements(By.linkText(fileName)).size() == 0;
	}

	/**
	 * Selects the file by name.
	 * 
	 * @param fileName
	 *            - name of file in the file list
	 */
	public void selectFile(String fileName) {
		assert (isFileInList(fileName));
		super.getDriver().findElement(By.linkText(fileName))
				.findElement(By.xpath("..")).click();
	}

	/**
	 * Inputs to focused field and submits.
	 * 
	 * @param text
	 *            - text to be input.
	 */
	public void inputToFocusedFieldAndSubmit(String text) {
		new Actions(super.getDriver()).sendKeys(text).perform();
		new Actions(super.getDriver()).sendKeys(Keys.RETURN).perform();
	}
}
