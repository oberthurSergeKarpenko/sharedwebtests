package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Sign In' popup window.
 */
public class SignInPage extends PageObject {

	@FindBy(xpath = "//input[@type='email']")
	private WebElement loginField;

	@FindBy(xpath = "//input[@type='password']")
	private WebElement passwordField;

	@FindBy(className = "sign-in-text")
	private WebElement signInBtn;

	/**
	 * Constructor. Used by Thucydides.
	 */
	public SignInPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(loginField).waitUntilVisible();
	}

	/**
	 * Inputs credentials.
	 * 
	 * @param login
	 * @param password
	 */
	public void inputCredentials(String login, String password) {
		loginField.clear();
		loginField.sendKeys(login);
		passwordField.clear();
		passwordField.sendKeys(password);
	}

	/**
	 * Clicks 'Sign In' button.
	 */
	public void clickSignInBtn() {
		signInBtn.click();
	}

}
