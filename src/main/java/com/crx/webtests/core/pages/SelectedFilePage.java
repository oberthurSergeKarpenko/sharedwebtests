package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.annotations.findby.FindBy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Files' page with selected file(Enabled
 *         actions with file).
 */
public class SelectedFilePage extends FilesPage {

	@FindBy(id = "delete_action_button")
	private WebElement deleteBtn;

	@FindBy(id = "rename_action_button")
	private WebElement renameBtn;

	/**
	 * Constructor. Used by Thucydides.
	 */
	public SelectedFilePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Method used to wait for page to load.
	 */
	@WhenPageOpens
	public void waitPageToLoad() {
		element(deleteBtn).waitUntilVisible();
	}

	/**
	 * Clicks 'Delete' button
	 */
	public void clickDeleteBtn() {
		deleteBtn.click();
	}

	/**
	 * Clicks 'Rename' button
	 */
	public void clickRenameBtn() {
		renameBtn.click();
	}

}
