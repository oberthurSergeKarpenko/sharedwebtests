package com.crx.webtests.core.pages;

import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author serge
 * 
 *         Class for all pages with Side Menu.
 */
public abstract class PageWithSideBar extends PageObject {

	@FindBy(xpath = "//a[@href='https://www.dropbox.com/home']")
	private WebElement filesLink;

	@FindBy(xpath = "//a[@href='/events']")
	private WebElement eventsLink;

	/**
	 * Constructor. Used by Thucydides.
	 */
	public PageWithSideBar(WebDriver driver) {
		super(driver);
	}

	/**
	 * Clicks Side Bar 'Events' button.
	 */
	public void clickEventsTab() {
		eventsLink.click();
	}

	/**
	 * Clicks Side Bar 'Files' button.
	 */
	public void clickFilesTab() {
		filesLink.click();
	}

}
