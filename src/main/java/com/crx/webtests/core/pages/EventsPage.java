package com.crx.webtests.core.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author serge
 * 
 *         Page Object for Dropbox 'Events' page.
 */
public class EventsPage extends PageWithSideBar {

	private String eventRowsXpath = "//tr[@class='event-row']";

	private WebDriverWait wait = new WebDriverWait(super.getDriver(), 30);

	/**
	 * Constructor. Used by Thucydides.
	 */
	public EventsPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Gets text of event by provided index.
	 * 
	 * @param index
	 *            - index of event in the events list. index = 0 - the latest
	 *            event.
	 * @return - all visible text of event in the vent list
	 */
	public String getEventText(int index) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath(eventRowsXpath + "[1]")));
		List<WebElement> eventRows = super.getDriver().findElements(
				By.xpath(eventRowsXpath));
		assert eventRows.size() >= index : "Events number is less than "
				+ index;
		return eventRows.get(index).getText();

	}
}
