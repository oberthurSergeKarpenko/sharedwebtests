package com.crx.webtests.core;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.crx.webtests.core.pages.DeleteFilePage;
import com.crx.webtests.core.pages.EventsPage;
import com.crx.webtests.core.pages.FilesPage;
import com.crx.webtests.core.pages.SelectedFilePage;
import com.crx.webtests.core.pages.SignInPage;
import com.crx.webtests.core.pages.StartPage;
import com.crx.webtests.core.pages.UploadPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

/**
 * @author serge
 * 
 *         Class that represents steps available for Dropbox user.
 */
public class DropboxUserSteps extends ScenarioSteps {

	/**
	 * Constructor. Used by Thucydides.
	 */
	public DropboxUserSteps(Pages pages) {
		super(pages);
	}

	@Step
	public void openStartPage() {
		StartPage startPage = getPages().get(StartPage.class);
		startPage.open();
	}

	@Step
	public void signIn(String login, String password) {
		StartPage startPage = getPages().get(StartPage.class);
		SignInPage signInPage = getPages().get(SignInPage.class);

		startPage.clickSignInLink();
		signInPage.inputCredentials(login, password);
		signInPage.clickSignInBtn();
	}

	@Step
	public void createFolder(String folderName) {
		FilesPage filesPage = getPages().get(FilesPage.class);

		filesPage.clickNewFolderBtn();
		filesPage.inputToFocusedFieldAndSubmit(folderName);
	}

	@Step
	public void assertFileInList(String fileName) {
		FilesPage filesPage = getPages().get(FilesPage.class);

		assertThat(filesPage.isFileInList(fileName), is(true));
	}

	@Step
	public void uploadFile(String filePath) {
		FilesPage filesPage = getPages().get(FilesPage.class);
		UploadPage uploadPage = getPages().get(UploadPage.class);

		filesPage.clickUploadBtn();
		uploadPage.uploadFileAndClickDone(filePath);
	}

	@Step
	public void navigateToEvents() {
		FilesPage filesPage = getPages().get(FilesPage.class);
		filesPage.clickEventsTab();
	}

	@Step
	public void navigateToFiles() {
		EventsPage eventsPage = getPages().get(EventsPage.class);
		eventsPage.clickFilesTab();
	}

	@Step
	public void checkEvent(String fileName, String fileType, int index) {
		EventsPage eventsPage = getPages().get(EventsPage.class);

		assertThat(eventsPage.getEventText(index),
				containsString("You added the " + fileType + " " + fileName));
	}

	@Step
	public void renameFile(String fileName, String newFileName) {
		FilesPage filesPage = getPages().get(FilesPage.class);
		SelectedFilePage selectedFilePage = getPages().get(
				SelectedFilePage.class);

		filesPage.selectFile(fileName);
		selectedFilePage.clickRenameBtn();
		selectedFilePage.inputToFocusedFieldAndSubmit(newFileName);
	}

	@Step
	public void deleteFile(String fileName) {
		FilesPage filesPage = getPages().get(FilesPage.class);
		SelectedFilePage selectedFilePage = getPages().get(
				SelectedFilePage.class);
		DeleteFilePage deleteFilePage = getPages().get(DeleteFilePage.class);

		filesPage.selectFile(fileName);
		selectedFilePage.clickDeleteBtn();
		deleteFilePage.clickDeleteBtn();
	}

	@Step
	public void assertFileNotInList(String fileName) {
		FilesPage filesPage = getPages().get(FilesPage.class);

		assertThat(filesPage.isFileNotInList(fileName), is(true));
	}
}
