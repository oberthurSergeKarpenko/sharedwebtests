package com.crx.webtests.requirements;

import net.thucydides.core.annotations.Feature;

/**
 * @author serge
 * 
 *         This is Thucydides "features" definition. Used to model the
 *         application requirements.
 */
public class Application {
	@Feature
	public class DropBox {
		public class CoreFunctions {
		}
	}

}
