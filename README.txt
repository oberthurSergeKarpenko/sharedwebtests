Hello!

I've chosen Thucydides framework for this test task as it's open source and 
provides all instruments to deal with UI tests in simple and elegant manner.
As well it provides good reporting(including test coverage based on described
requirements), ease to integrate with CI and a lot of other useful stuff :)
Also it is a good match to requirements that I got because it uses
Selenium and allows the user to utilize Page Object pattern.


Preconditions:
1. Connectivity to https://www.dropbox.com/
2. Pre-created DropBox account.
3. 'Files' folder is empty. Scrolling is not supported by tests.


REQUIREMENTS for this project:
- Maven 3.0.x
- Java 5 or newer

To RUN test it should be enough:
1. Download 'chromedriver' accrodingly to your OS from:
	http://chromedriver.storage.googleapis.com/index.html?path=2.14/
	and put it to '${projectDir}/src/main/resources/'. Make sure that your 
	driver is executable (do 'chmod +x .../chromedriver' if you on Linux).
2. Execute 'mvn test -Dwebdriver.chrome.driver="src/main/resources/chromedriver" -Ddropbox.login="" -Ddropbox.password="" thucydides:aggregate'.
	(Specify your driver name - e.g. '.../chromedriver.exe' if you are on Windows.
	 Specify your Dropbox login and password)


REPORTS can be found:
1. ${projectDir}/target/site/thucydides/index.html  - for HTML report with screenshots and fancy charts :)
2. ${projectDir}/target/surefire-reports - for JUnit style report.


Tested OS: Mac OS v.10.9.3
Tested browser: Chrome v.42


I really appreciate your time for reviewing this!

Thank you and have a nice day!

--
Serge Karpenko